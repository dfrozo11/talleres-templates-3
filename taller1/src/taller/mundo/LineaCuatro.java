package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	public final static String POSICION_LIBRE= "___";
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;


	private int fichasjuntas; 
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol,int fichas)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		fichasjuntas= fichas;
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		int aleatorio = (int) Math.round(Math.random()*(tablero.length-1)) ;
		
		if(!registrarJugada(aleatorio+1))
		{
			registrarJugadaAleatoria();
		}
	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		int columna = col-1;
		int cambios =0;
		for ( int i =tablero[0].length-1; i>=0 && cambios==0; i--)
		{
			if(tablero[i][columna].equals(POSICION_LIBRE))
			{
				tablero[i][columna]="_"+jugadores.get(turno).darSimbolo()+"_";
				finJuego=terminar(i, columna);
				cambios +=1;
				if(!finJuego)
				{
				 turno+=1;
				}
				
			}
		}
		if (turno == jugadores.size())
		{
			turno=0;
		}
		atacante=jugadores.get(turno).darNombre();
		return cambios!=0;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		int cont1=0; 
		boolean termino = false; 
		for(int i = fil ; i<tablero.length; i++)
		{
			if(tablero[i][col].equals(tablero[fil][col]))
			{
				cont1++;
				if(cont1==fichasjuntas)
				{
					return true;
				}
			}
			else
			{
				cont1=0;
			}

		}
		cont1=0;
		for(int i =col; i<tablero[0].length; i++)
		{
			if(tablero[fil][i].equals(tablero[fil][col]))
			{
				cont1++;
				if(cont1==fichasjuntas)
				{
					return true;
				}
			}
			else
			{
				cont1=0;
			}
		}

		cont1=0;
		for(int i =col; i>=0; i--)
		{
			if(tablero[fil][i].equals(tablero[fil][col]))
			{
				cont1++;
				if(cont1==fichasjuntas)
				{
					return true;
				}
			}
			else
			{
				cont1=0;
			}
		}

		termino= ganoDiagonales(fil, col);
		return termino;
	}

	public boolean ganoDiagonales(int fil,int col)
	{
		int cont=4; 
		int j =0;
		int cont1=0;
		int cont2=0;
		int cont3=0;
		int cont4=0;

		while(cont!=0)
		{
			cont=0;

			if(fil+j < tablero.length && col+j<tablero[0].length)
			{
				cont++;
				if(tablero[fil+j][col+j].equals(tablero[fil][col]))
				{
					cont1++;
				}
				else
				{
					cont1=0;
				}
			}
			if(fil+j < tablero.length && col-j>=0)
			{
				cont++;
				if(tablero[fil+j][col-j].equals(tablero[fil][col]))
				{
					cont2++;
				}
				else
				{
					cont2=0;
				}
			}
			if(fil-j>=0  && col+j< tablero.length)
			{
				cont++;
				if(tablero[fil-j][col+j].equals(tablero[fil][col]))
				{
					cont3++;
				}
				else
				{
					cont3=0;
				}
			}
			if(fil-j>=0  && col+j< tablero.length)
			{
				cont++;
				if(tablero[fil-j][col+j].equals(tablero[fil][col]))
				{
					cont3++;
				}
				else
				{
					cont3=0;
				}
			}
			if(fil-j>=0  && col-j>=0)
			{
				cont++;
				if(tablero[fil-j][col-j].equals(tablero[fil][col]))
				{
					cont4++;
				}
				else
				{
					cont4=0;
				}
			}
			if(cont1==fichasjuntas||cont2==fichasjuntas||cont3==fichasjuntas||cont1==fichasjuntas)
			{
				return true;
			}
			j++;
		}

		return false;
	}


}
