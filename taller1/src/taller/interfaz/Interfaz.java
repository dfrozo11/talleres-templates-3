package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
					if(juego.fin())
					{
						break;
					}
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
					if(juego.fin())
					{
						break;
					}
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{	
		int filas =0;
		int colum =0;
		System.out.println("Ingrese el numero de jugadores");
		int resp = Integer.parseInt(sc.next());
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		
		for(int i =0; i < resp; i++)
		{
			System.out.println("Ingrese el nombre del jugador " + (i+1));
			String nombre = sc.next();
			String simbolo = "";
			while(true)
			{
				System.out.println("Ingrese el simbolo del usuario");
				simbolo =sc.next();
				if(simbolo.toCharArray().length==1)
				{
					break;
				}
				else
				{
					System.out.println("Ingrese solo un caracter");
					
				}
			}
			jugadores.add(new Jugador(nombre , simbolo));
				
		}

		System.out.println("Ingrese el numero de fichas juntas con las que gana el juego");
		int fichas= Integer.parseInt(sc.next());

		while(true)
		{
			System.out.println("Ingrese el numero de filas seguido por una coma y el numero de columnas");
			String[] dim = sc.next().split(",");
			filas = Integer.parseInt(dim[0]);
			colum = Integer.parseInt(dim[1]);
			if(filas>=fichas && colum>=fichas)
			{
				break;
			}
			System.out.println("Las dimensiones minimas del tablero son " + fichas +" x "+ fichas);
		}

		juego = new LineaCuatro(jugadores,filas, colum, fichas);
		juego();

	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		System.out.println("Es el turno de " + juego.darAtacante());
		System.out.println("Ingrese el numero de la columna a jugar");
		imprimirTablero();
		try
		{
			int col =  sc.nextInt();
			if (!juego.registrarJugada(col))
			{
				System.out.println("Movimiento invalido");
				juego();
			}
			else
			{
				if(juego.fin())
				{
					System.out.println("Gana "+juego.darAtacante()+" !!!");
					imprimirTablero();
					while(true)
					{
						System.out.println("¿Desea volver a jugar?");	
						System.out.println("1.Si");	
						System.out.println("2.No");	
						int opt = Integer.parseInt(sc.next());
						if(opt ==1 )
						{
							empezarJuegoMaquina();
							break;
						}
						else if(opt ==2)
						{
							System.out.println("¡Vuelva pronto!");
							break;
						}
						else
						{
							System.out.println("Comando inválido");

						}

					}

				}
				else
				{
					if(juego.darAtacante().equals("la computadora"))
					{
						juegoMaquina();
					}
					else
					{
						juego();
					}

				}
			}
		}
		catch(Exception e)
		{
			System.out.println("Comando inválido");
			juego();
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		int filas=0;
		int colum=0;
		String simbolo = "";
		System.out.println("Ingrese el nombre del usuario:");
		String nombre=sc.next();
		
		while(true)
		{
			System.out.println("Ingrese el simbolo del usuario");
			simbolo =sc.next();
			if(simbolo.toCharArray().length==1)
			{
				break;
			}
			else
			{
				System.out.println("Ingrese solo un caracter");
				
			}
		}
		
		System.out.println("Ingrese el numero de fichas juntas con las que gana el juego");
		int fichas= Integer.parseInt(sc.next());
		while(true)
		{
			System.out.println("Ingrese el numero de filas seguido por una coma y el numero de columnas");
			String[] dim = sc.next().split(",");
			filas = Integer.parseInt(dim[0]);
			colum = Integer.parseInt(dim[1]);
			if(filas>=fichas && colum>=fichas)
			{
				break;
			}
			System.out.println("Las dimensiones minimas del tablero son " + fichas +" x "+ fichas);
		}

		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		jugadores.add(new Jugador(nombre, simbolo));
		jugadores.add(new Jugador("la computadora", "C"));
		juego = new LineaCuatro(jugadores, filas, colum, fichas);
		juego();

	}
	
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		juego.registrarJugadaAleatoria();
		if(juego.fin())
		{
			System.out.println("Gana "+juego.darAtacante()+" !!!");
			imprimirTablero();
			while(true)
			{
				System.out.println("¿Desea volver a jugar?");	
				System.out.println("1.Si");	
				System.out.println("2.No");	
				int opt = Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuegoMaquina();
					break;
				}
				else if(opt==2)
				{
					System.out.println("¡Vuelva pronto!");
					System.exit(0);
					
					break;
				}
				else
				{
					System.out.println("Comando inválido");

				}

			}

		}
		else
		{
			juego();
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] tablero =juego.darTablero();
		for ( int i = 0 ; i < tablero.length; i++)
		{
			String linea ="";
			for ( int  j = 0; j<tablero[0].length; j++)
			{
				linea+=tablero[i][j]+" ";
			}
			System.out.println(linea);
		}

	}
}
